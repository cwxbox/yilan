const db = uniCloud.database()
const music = db.collection('music')

exports.main = async (event) => {
	let params = event.params //传入值
	let res
	switch (event.action) {
		case 'get':
			let row = await music.limit(1).get()
			res = {
				code: 0,
				data: row.data[0]
			}
			break;
		default:
			res = {
				code: 403,
				msg: '非法访问'
			}
			break;
	}
	return res
}
